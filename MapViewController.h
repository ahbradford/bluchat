//
//  MapViewController.h
//  Peripheral
//
//  Created by adambradford on 4/14/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *map;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel2;
@property (nonatomic) CLLocation* location;
@property (nonatomic) NSDate *date;
@property (nonatomic) NSString *message;

@end
