;//
//  ViewController.m
//  Quarters
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "ViewController.h"
#import "CentralBTController.h"
#import "PeripheralBTController.h"
#import "MessageTableViewController.h"
#import "Message+Create.h"	
#import <AudioToolbox/AudioToolbox.h>
#import "ABColorPicker.h"
#import "ColorChangeButton.h"
#import<CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>

@interface ViewController () <CentralBTControllerDelegate,PeripheralBTControllerDelegate,UITextFieldDelegate,CLLocationManagerDelegate>
@property (strong,nonatomic) CentralBTController       *central;
@property (strong,nonatomic) PeripheralBTController    *peripheral;
@property (strong,nonatomic) PeripheralBTController    *peripheral2;
@property                    SystemSoundID              sonic;
@property (strong,nonatomic) CLLocationManager			*locationManager;
@property (strong,nonatomic) CLLocation					*currentLocation;
@property (strong,nonatomic) NSTimer					*resetTimer;
@property (strong,nonatomic) MessageTableViewController *tableViewController;
@property (strong)			 NSOperationQueue			*sendQueue;
@property (strong,nonatomic) ABColorPicker				*colorPicker;


@end

@implementation ViewController

-(void)setBubbleColor:(UIColor *)bubbleColor
{
	_bubbleColor = bubbleColor;
	_peripheral.bubbleColor = bubbleColor;
	_colorSettingsButton.color = bubbleColor;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	//create operations queue that allows only 1 operation at a time
    _sendQueue = [[NSOperationQueue alloc]init];
	_sendQueue.maxConcurrentOperationCount = 1;
	
	//create central BT controller
	_central = [[CentralBTController alloc]init];
	_central.delegate = self;
	
	//create our tableview
	_tableViewController = [[MessageTableViewController alloc]initWithStyle:UITableViewStylePlain];
	_tableViewController.view = _messageView;
	_tableViewController.currentNavagationController = self.navigationController;
	
	//These are needed to release the keyboard on selection of a row.
	_tableViewController.textView1 = _textField;
	_tableViewController.textView2 = _nameTextField;
	
	//set up tableview delegates
	_messageView.dataSource = _tableViewController;
	_messageView.delegate = _tableViewController;
	
	//get text field return information
	_textField.delegate = self;
	
	//initialize bluetooth
	[self reInitalizeBluetoothAndLocation];
	
	[_textViewBorder setContentMode:UIViewContentModeRedraw];
	
	
	_nameTextField.layer.shadowColor = [UIColor blackColor].CGColor;
	_nameTextField.layer.shadowOpacity = 1;
	_nameTextField.layer.shadowRadius = 1.0;
	_nameTextField.layer.shadowOffset = CGSizeMake(1.0, 1.0);
   
	//load sound
    SystemSoundID sound;
    AudioServicesCreateSystemSoundID((CFURLRef)CFBridgingRetain([NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"sonicRing" ofType:@"mp3"]]), &sound);
    self.sonic = sound;
	
	if(!_bubbleColor) self.bubbleColor = [UIColor colorWithRed: 0.467 green: 0.646 blue: 0.9 alpha: 1];
	
		
	
	
	//subscribe to keyboard appear and disappear events
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector (keyboardWillShow:)
												 name: UIKeyboardWillShowNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector (keyboardWillHide:)
												 name: UIKeyboardWillHideNotification object:nil];

}

-(void)viewWillAppear:(BOOL)animated
{
	
	[self.view layoutIfNeeded];
	//hide the navagation bar 
	[self.navigationController setNavigationBarHidden:YES animated:NO];
	
	[_messageView reloadData];
	
	//scroll text to bottom after enough time passes for the core database to load
	double delayInSeconds = .2;
	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
	dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		[self scrollTextToBottom];
		
	});
	
	//reinitalze bluetooth again, just in case
	[self reInitalizeBluetoothAndLocation];
	
	[_colorSettingsButton setNeedsDisplay];
	
	_colorPicker = nil;
}


//moves input text field with keyboard
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
	CGFloat width = keyboardFrame.size.width;
	
	UIInterfaceOrientation interfaceOrientation = self.interfaceOrientation;
	
	
	if(interfaceOrientation == UIDeviceOrientationLandscapeLeft)
		
		self.keyboardHeight.constant = -width ;
	else if (interfaceOrientation == UIDeviceOrientationLandscapeRight)
		self.keyboardHeight.constant = -width ;
	else
		self.keyboardHeight.constant = -height;
	   
	
	[self.view setNeedsUpdateConstraints];  
	
	[UIView animateWithDuration:animationDuration animations:^{
		[self.view layoutIfNeeded];
	if([_messageView numberOfRowsInSection:0]>1)	dispatch_async(dispatch_get_main_queue(), ^() {
			[self scrollTextToBottom];});
		
			[self.view layoutIfNeeded];
		
	}];
}


//moves input text field with keyboard
- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
   
	UIInterfaceOrientation interfaceOrientation = self.interfaceOrientation;
	
	if(interfaceOrientation == UIDeviceOrientationLandscapeLeft)
		
		self.keyboardHeight.constant = 0;
	else if (interfaceOrientation == UIDeviceOrientationLandscapeRight)
		self.keyboardHeight.constant = 0;
	else
		self.keyboardHeight.constant = 0;
	
    
	[self.view setNeedsUpdateConstraints];
	
	[UIView animateWithDuration:animationDuration animations:^{
		[self.view layoutIfNeeded];
	}];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
	[textField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//adds message to database, which will then automatically update the tableview
-(void)addMessageToModel:(NSString *)text received:(BOOL)received red:(float)r green:(float)g blue:(float)b
{
	
	[_tableViewController.managedObjectContext performBlock:^{
		
		double lat = _currentLocation.coordinate.latitude;
		double lon = _currentLocation.coordinate.longitude;
		[Message messageWithText:text received:received latitude:lat longitude:lon date:[NSDate date] red:r green:g blue:b inManagedObjectContext:_tableViewController.managedObjectContext];
	}];
}

//handles received messages
-(void)dataReceivedfromPeripherial:(NSData *)data withColorR:(float)r colorG:(float)g colorB:(float)b
{
   
	NSString *text = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding ];
	[self addMessageToModel:text received:YES red:r green:g blue:b];
	
    AudioServicesPlayAlertSound(self.sonic);
}


- (IBAction)send
{
	//reinitialize if needed
	if(!_peripheral || !_central || !_locationManager)
	{
		[self reInitalizeBluetoothAndLocation];
	}
	
	// 0 lenght messages make messed up text bubbles
	if(_textField.text.length < 1)return;
    
    //Set up message to be sent
	NSString *message;
	
	//if the user has entered a name, append it
	if(_nameTextField.text.length > 0)
	{
		message = [NSString stringWithFormat:@"%@: %@\n",_nameTextField.text, _textField.text];
	}
	else
	{
		message = _textField.text;
	}
	
	//this handles tiny string sizes...
	NSString *modelMessage = _textField.text;
	if(modelMessage.length == 1)modelMessage = [modelMessage stringByAppendingString:@" "];
	
	if(_bubbleColor)
	{
		const float* colors = CGColorGetComponents( _bubbleColor.CGColor );
		[self addMessageToModel:modelMessage received:NO red:colors[0] green:colors[1] blue:colors[2]];
		
	}
	else
	{
		
		[self addMessageToModel:modelMessage received:NO red:.467 green:.646 blue:.9];
	}
	
    
	//Add our send message to the queue, which will process them in order, synchronously, update the indicator
	[_sendQueue addOperationWithBlock:^
	 {
		 
		 
		 [[NSOperationQueue mainQueue] addOperationWithBlock:^
		  {
			  
			  [_infoButton setHidden:YES];
			  [_activityIndicator startAnimating];}];
		 
		 [_peripheral sendDatatoCentral:[message dataUsingEncoding:NSUTF8StringEncoding]];
		 
		 //after no messages are left update the infobutton/activity indicator
		 if(_sendQueue.operations.count > 0)
		 {
			 //update gue information after send completes
			 [[NSOperationQueue mainQueue] addOperationWithBlock:^
			 {
				 [_activityIndicator stopAnimating];
				 [_infoButton setHidden:NO];
			 }];
			 
		 }
	 }];
	
	//reset textfield text.
	_textField.text = @"";

}

//resets peripheral maager and restarts it after 1 second.
-(void)resetPeripheralManager
{
	//resets preipheral manager
	[_peripheral stopAdvertising];
	[_peripheral removeServices];
	_peripheral = nil;
	
	//wait 1 second and restart
	double delayInSeconds = 1.0;
	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
	dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
	{
		_peripheral = [[PeripheralBTController alloc]init];
		_peripheral.delegate = self;
		_peripheral.bubbleColor = _bubbleColor;
	});
}

//scrolls tableview to the bottom
-(void)scrollTextToBottom
{
	
	if([_messageView numberOfRowsInSection:0] == NSIntegerMax || [_messageView numberOfRowsInSection:0] == 0)
	{
		return;
	}
	NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:([_messageView numberOfRowsInSection:([_messageView numberOfSections] - 1)] - 1) inSection:([_messageView numberOfSections] - 1)];
	[_messageView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark UITextFieldDelegate methods

//if text field hits return
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self send];
    return YES;
}

#pragma mark ABCMObserverDelegate methods


//save location information
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	_currentLocation = [locations lastObject];
	//NSLog(@"%@",_currentLocation);
}

//shuts down perhipheral, central and location manager
-(void)shutDownServices
{
	[_peripheral stopAdvertising];
	_peripheral = nil;
	_central = nil;
	
	[_locationManager stopMonitoringSignificantLocationChanges];
}

//initalaizes peripheral, central and location manager if needed
-(void)reInitalizeBluetoothAndLocation
{
	
	
	if(!_locationManager)
	{
	_locationManager = [[CLLocationManager alloc]init];
	_locationManager.delegate = self;
	}
	
	[_locationManager startMonitoringSignificantLocationChanges];
	
	if(!_peripheral)
	{
		_peripheral = [[PeripheralBTController alloc]init];
		_peripheral.delegate = self;
	}
	
	if(!_central)
	{
		_central = [[CentralBTController alloc]init];
		_central.delegate = self;
	}
}

//re adjust display on rotations
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
	[_messageView reloadData];
	[_textViewBorder setNeedsDisplay];
	[self scrollTextToBottom];
}

//shows an information popover
- (IBAction)settingsButtonPressed:(id)sender
{
	
	self.navigationController.navigationBar.tintColor = [UIColor darkGrayColor];
	
	_colorPicker = [[ABColorPicker alloc]init];
	_colorPicker.translatesAutoresizingMaskIntoConstraints = NO;
	[_colorPicker setContentMode:UIViewContentModeRedraw];
	
	UIViewController *vc = [[UIViewController alloc]init];
	[vc.view addSubview:_colorPicker];
	

	
	[vc.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_colorPicker]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_colorPicker)]];
	
	[vc.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_colorPicker]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_colorPicker)]];
	
	[_colorPicker addTarget:self action:@selector(colorPickerChanged) forControlEvents:UIControlEventValueChanged];
	
	
	
	[self.navigationController setNavigationBarHidden:NO];
	[self.navigationController	pushViewController:vc animated:YES];
	
}

- (IBAction)infoButtonPressed:(id)sender

{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BluChat Instructions "
													message:@"BluChat Requires a Bluetooth 4.0 capable device.\n  No pairing is required.\nBluChat cannot receive messages while in the background.\n Tap on the Name at the top to change it.\nTap on any message to see when and where it was received."
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}

-(void)colorPickerChanged
{
	self.bubbleColor = _colorPicker.color;
	
}

@end
