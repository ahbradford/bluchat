//
//  Message.m
//  Peripheral
//
//  Created by adambradford on 4/7/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "Message.h"


@implementation Message

@dynamic messageString;
@dynamic latitude;
@dynamic date;
@dynamic longitude;
@dynamic received;
@dynamic red;
@dynamic green;
@dynamic blue;

@end
