//
//  BubbleView.m
//  Peripheral
//
//  Created by adambradford on 4/20/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "BubbleView.h"

@implementation BubbleView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

//code from paintcode
- (void)drawRect:(CGRect)rect
{
	//if the bubble is to be pointing left
	if(_orientation == 0)
	{
		//// General Declarations
		CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		CGContextRef context = UIGraphicsGetCurrentContext();
		
		//// Color Declarations
		UIColor* baseColor = _color;
		CGFloat baseColorRGBA[4];
		[baseColor getRed: &baseColorRGBA[0] green: &baseColorRGBA[1] blue: &baseColorRGBA[2] alpha: &baseColorRGBA[3]];
		
		UIColor* gradientDerivedColor = [UIColor colorWithRed: (baseColorRGBA[0] * 0.2 + 0.8) green: (baseColorRGBA[1] * 0.2 + 0.8) blue: (baseColorRGBA[2] * 0.2 + 0.8) alpha: (baseColorRGBA[3] * 0.2 + 0.8)];
		UIColor* shadow1 = [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 1];
		
		//// Gradient Declarations
		NSArray* gradientColors = [NSArray arrayWithObjects:
								   (id)gradientDerivedColor.CGColor,
								   
								   (id)baseColor.CGColor, nil];
		CGFloat gradientLocations[] = {0,  0.54};
		CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);
		
		//// Shadow Declarations
		UIColor* shadow = shadow1;
		CGSize shadowOffset = CGSizeMake(1.1, 1.1);
		CGFloat shadowBlurRadius = 2.5;
		
		//// Frames
		CGRect frame = self.bounds;
		
		
		//// Bezier Drawing
		UIBezierPath* bezierPath = [UIBezierPath bezierPath];
		[bezierPath moveToPoint: CGPointMake(CGRectGetMinX(frame) + 1.1, CGRectGetMinY(frame) + 16.5)];
		[bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 1.1, CGRectGetMaxY(frame) - 17.5)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 16.1, CGRectGetMaxY(frame) - 2.5) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 1.1, CGRectGetMaxY(frame) - 9.22) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 7.81, CGRectGetMaxY(frame) - 2.5)];
		[bezierPath addLineToPoint: CGPointMake(CGRectGetMaxX(frame) - 24.9, CGRectGetMaxY(frame) - 2.5)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 15.73, CGRectGetMaxY(frame) - 5.63) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 21.45, CGRectGetMaxY(frame) - 2.5) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 18.27, CGRectGetMaxY(frame) - 3.67)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 5.9, CGRectGetMaxY(frame) - 2.5) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 13.77, CGRectGetMaxY(frame) - 4.16) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 10.67, CGRectGetMaxY(frame) - 2.77)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 12.51, CGRectGetMaxY(frame) - 9.05) controlPoint1: CGPointMake(CGRectGetMaxX(frame) + 4.7, CGRectGetMaxY(frame) - 1.91) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 6.4, CGRectGetMaxY(frame) - 2.33)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 9.9, CGRectGetMaxY(frame) - 17.5) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 10.86, CGRectGetMaxY(frame) - 11.46) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 9.9, CGRectGetMaxY(frame) - 14.37)];
		[bezierPath addLineToPoint: CGPointMake(CGRectGetMaxX(frame) - 9.9, CGRectGetMinY(frame) + 16.5)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 24.9, CGRectGetMinY(frame) + 1.5) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 9.9, CGRectGetMinY(frame) + 8.22) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 16.62, CGRectGetMinY(frame) + 1.5)];
		[bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 16.1, CGRectGetMinY(frame) + 1.5)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 1.1, CGRectGetMinY(frame) + 16.5) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 7.81, CGRectGetMinY(frame) + 1.5) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 1.1, CGRectGetMinY(frame) + 8.22)];
		[bezierPath closePath];
		CGContextSaveGState(context);
		CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, shadow.CGColor);
		[baseColor setFill];
		[bezierPath fill];
		CGContextRestoreGState(context);
		
		[shadow1 setStroke];
		bezierPath.lineWidth = 0.5;
		[bezierPath stroke];
		
		
		//// Rounded Rectangle 2 Drawing
		UIBezierPath* roundedRectangle2Path = [UIBezierPath bezierPath];
		[roundedRectangle2Path moveToPoint: CGPointMake(CGRectGetMaxX(frame) - 17, CGRectGetMinY(frame) + 16.5)];
		[roundedRectangle2Path addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 26.5, CGRectGetMinY(frame) + 31) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 17, CGRectGetMinY(frame) + 24.51) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 21.25, CGRectGetMinY(frame) + 31)];
		[roundedRectangle2Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 16, CGRectGetMinY(frame) + 31)];
		[roundedRectangle2Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 7, CGRectGetMinY(frame) + 16.5) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 10.75, CGRectGetMinY(frame) + 31) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 7, CGRectGetMinY(frame) + 24.51)];
		[roundedRectangle2Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 7, CGRectGetMinY(frame) + 16.5)];
		[roundedRectangle2Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 16.5, CGRectGetMinY(frame) + 2) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 7, CGRectGetMinY(frame) + 8.49) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 11.25, CGRectGetMinY(frame) + 2)];
		[roundedRectangle2Path addLineToPoint: CGPointMake(CGRectGetMaxX(frame) - 26.5, CGRectGetMinY(frame) + 2)];
		[roundedRectangle2Path addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 17, CGRectGetMinY(frame) + 16.5) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 21.25, CGRectGetMinY(frame) + 2) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 17, CGRectGetMinY(frame) + 8.49)];
		[roundedRectangle2Path closePath];
		CGContextSaveGState(context);
		[roundedRectangle2Path addClip];
		CGRect roundedRectangle2Bounds = CGPathGetPathBoundingBox(roundedRectangle2Path.CGPath);
		CGContextDrawLinearGradient(context, gradient,
									CGPointMake(CGRectGetMidX(roundedRectangle2Bounds), CGRectGetMinY(roundedRectangle2Bounds)),
									CGPointMake(CGRectGetMidX(roundedRectangle2Bounds), CGRectGetMaxY(roundedRectangle2Bounds)),
									0);
		CGContextRestoreGState(context);
		
		
		//// Cleanup
		CGGradientRelease(gradient);
		CGColorSpaceRelease(colorSpace);
		
	}
		

	//if the bubble is to be pointing right
	else
	{//// General Declarations
	
		//// General Declarations
		CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		CGContextRef context = UIGraphicsGetCurrentContext();
		
		//// Color Declarations
		UIColor* baseColor = _color;
		CGFloat baseColorRGBA[4];
		[baseColor getRed: &baseColorRGBA[0] green: &baseColorRGBA[1] blue: &baseColorRGBA[2] alpha: &baseColorRGBA[3]];
		
		UIColor* gradientDerivedColor = [UIColor colorWithRed: (baseColorRGBA[0] * 0.2 + 0.8) green: (baseColorRGBA[1] * 0.2 + 0.8) blue: (baseColorRGBA[2] * 0.2 + 0.8) alpha: (baseColorRGBA[3] * 0.2 + 0.8)];
		UIColor* shadow1 = [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 1];
		
		//// Gradient Declarations
		NSArray* gradientColors = [NSArray arrayWithObjects:
								   (id)gradientDerivedColor.CGColor,
								 
								   (id)baseColor.CGColor, nil];
		CGFloat gradientLocations[] = {0, 0.54};
		CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);

		//// Shadow Declarations
		UIColor* shadow = shadow1;
		CGSize shadowOffset = CGSizeMake(1.1, 1.1);
		CGFloat shadowBlurRadius = 2.5;
		
		//// Frames
		CGRect frame = self.bounds;
		
		
		//// Bezier Drawing
		UIBezierPath* bezierPath = [UIBezierPath bezierPath];
		[bezierPath moveToPoint: CGPointMake(CGRectGetMaxX(frame) - 1.5, CGRectGetMinY(frame) + 16.5)];
		[bezierPath addLineToPoint: CGPointMake(CGRectGetMaxX(frame) - 1.5, CGRectGetMaxY(frame) - 17.5)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 16.5, CGRectGetMaxY(frame) - 2.5) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 1.5, CGRectGetMaxY(frame) - 9.22) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 8.22, CGRectGetMaxY(frame) - 2.5)];
		[bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 24.5, CGRectGetMaxY(frame) - 2.5)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 15.33, CGRectGetMaxY(frame) - 5.63) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 21.05, CGRectGetMaxY(frame) - 2.5) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 17.86, CGRectGetMaxY(frame) - 3.67)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 5.5, CGRectGetMaxY(frame) - 2.5) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 13.36, CGRectGetMaxY(frame) - 4.16) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 10.27, CGRectGetMaxY(frame) - 2.77)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 12.11, CGRectGetMaxY(frame) - 9.05) controlPoint1: CGPointMake(CGRectGetMinX(frame) - 5.1, CGRectGetMaxY(frame) - 1.91) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 6, CGRectGetMaxY(frame) - 2.33)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 9.5, CGRectGetMaxY(frame) - 17.5) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 10.46, CGRectGetMaxY(frame) - 11.46) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 9.5, CGRectGetMaxY(frame) - 14.37)];
		[bezierPath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 9.5, CGRectGetMinY(frame) + 16.5)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 24.5, CGRectGetMinY(frame) + 1.5) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 9.5, CGRectGetMinY(frame) + 8.22) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 16.22, CGRectGetMinY(frame) + 1.5)];
		[bezierPath addLineToPoint: CGPointMake(CGRectGetMaxX(frame) - 16.5, CGRectGetMinY(frame) + 1.5)];
		[bezierPath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 1.5, CGRectGetMinY(frame) + 16.5) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 8.22, CGRectGetMinY(frame) + 1.5) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 1.5, CGRectGetMinY(frame) + 8.22)];
		[bezierPath closePath];
		CGContextSaveGState(context);
		CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, shadow.CGColor);
		[baseColor setFill];
		[bezierPath fill];
		CGContextRestoreGState(context);
		
		[shadow1 setStroke];
		bezierPath.lineWidth = 0.5;
		[bezierPath stroke];
		
		
		//// Rounded Rectangle 2 Drawing
		UIBezierPath* roundedRectangle2Path = [UIBezierPath bezierPath];
		[roundedRectangle2Path moveToPoint: CGPointMake(CGRectGetMinX(frame) + 16, CGRectGetMinY(frame) + 16.5)];
		[roundedRectangle2Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 25.5, CGRectGetMinY(frame) + 31) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 16, CGRectGetMinY(frame) + 24.51) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 20.25, CGRectGetMinY(frame) + 31)];
		[roundedRectangle2Path addLineToPoint: CGPointMake(CGRectGetMaxX(frame) - 17, CGRectGetMinY(frame) + 31)];
		[roundedRectangle2Path addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 8, CGRectGetMinY(frame) + 16.5) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 11.75, CGRectGetMinY(frame) + 31) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 8, CGRectGetMinY(frame) + 24.51)];
		[roundedRectangle2Path addLineToPoint: CGPointMake(CGRectGetMaxX(frame) - 8, CGRectGetMinY(frame) + 16.5)];
		[roundedRectangle2Path addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 17.5, CGRectGetMinY(frame) + 2) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 8, CGRectGetMinY(frame) + 8.49) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 12.25, CGRectGetMinY(frame) + 2)];
		[roundedRectangle2Path addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 25.5, CGRectGetMinY(frame) + 2)];
		[roundedRectangle2Path addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 16, CGRectGetMinY(frame) + 16.5) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 20.25, CGRectGetMinY(frame) + 2) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 16, CGRectGetMinY(frame) + 8.49)];
		[roundedRectangle2Path closePath];
		CGContextSaveGState(context);
		[roundedRectangle2Path addClip];
		CGRect roundedRectangle2Bounds = CGPathGetPathBoundingBox(roundedRectangle2Path.CGPath);
		CGContextDrawLinearGradient(context, gradient,
									CGPointMake(CGRectGetMidX(roundedRectangle2Bounds), CGRectGetMinY(roundedRectangle2Bounds)),
									CGPointMake(CGRectGetMidX(roundedRectangle2Bounds), CGRectGetMaxY(roundedRectangle2Bounds)),
									0);
		CGContextRestoreGState(context);
		
		
		//// Cleanup
		CGGradientRelease(gradient);
		CGColorSpaceRelease(colorSpace);
		
		
	}

}


@end
