//
//  MapViewController.m
//  Peripheral
//
//  Created by adambradford on 4/14/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "MapViewController.h"
#import <QuartzCore/QuartzCore.h>


@interface MapViewController ()

@property NSOperationQueue* queue;

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_map.showsUserLocation = YES;
	_queue = [[NSOperationQueue alloc]init];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
	//sets up map visual settings
	self.navigationController.navigationBar.tintColor = [UIColor darkGrayColor];
	
	[_map.layer setCornerRadius:10.0f];
	[_map.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	[_map.layer setBorderWidth:1.5f];
	[_map.layer setShadowColor:[UIColor blackColor].CGColor];
	[_map.layer setShadowOpacity:0];
	[_map.layer setShadowRadius:5.0];
	[_map.layer setShadowOffset:CGSizeMake(4.0, 4.0)];
	
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	
	//Set title
	self.navigationItem.title = _message;
	
	
	//Create and display date with layer styles
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
	dateFormatter.dateFormat = @"EEEE hh:mm a dd MMM, YYYY";
	_dateLabel.text = [dateFormatter stringFromDate:_date];
	
	[_dateLabel.layer setShadowColor:[UIColor whiteColor].CGColor];
	[_dateLabel.layer setShadowOpacity:1];
	[_dateLabel.layer setShadowRadius:1.0];
	[_dateLabel.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
	
	[_addressLabel.layer setShadowColor:[UIColor whiteColor].CGColor];
	[_addressLabel.layer setShadowOpacity:1];
	[_addressLabel.layer setShadowRadius:1.0];
	[_addressLabel.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
	
	[_addressLabel2.layer setShadowColor:[UIColor whiteColor].CGColor];
	[_addressLabel2.layer setShadowOpacity:1];
	[_addressLabel2.layer setShadowRadius:1.0];
	[_addressLabel2.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
	
	
	//Test if we have a location
	if(_location.coordinate.latitude == 0 || _location.coordinate.longitude == 0)
	{
		_addressLabel.text = @"No Location Information Available";
		_addressLabel2.text = @"No Location Information Available";
		
		return;
	}
	
	//set map region and map settings and drop a pin
	MKCoordinateRegion region;
	region.center = _location.coordinate;
	
	region.span =  MKCoordinateSpanMake(.02f, .02f);
	
	_map.mapType = MKMapTypeHybrid;
	
	MKPointAnnotation *pin = [[MKPointAnnotation alloc]init];
	pin.coordinate = _location.coordinate;
	pin.title = _message;
	pin.subtitle = _date.description;
	
	id userLocation = [_map userLocation];
	
	//remove any previous pins
    NSMutableArray *pins = [[NSMutableArray alloc] initWithArray:[_map annotations]];
    if ( userLocation != nil ) {
        [pins removeObject:userLocation]; // avoid removing user location off the map
    }
	
    [_map removeAnnotations:pins];
	[_map addAnnotation:pin];
	
	[_map setRegion:region];
	
	CLLocation *location = [[CLLocation alloc]initWithCoordinate:_location.coordinate altitude:0 horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:0];
	
	
	CLGeocoder *geocoder = [[CLGeocoder alloc] init];
	
	//go out to web and find reverse geocode location info and set when it returns
	[_queue addOperationWithBlock:^{
		[geocoder reverseGeocodeLocation: location completionHandler: ^(NSArray *placemarks, NSError *error)
		 {
			 CLPlacemark *placemark = [placemarks objectAtIndex:0];

		 
			 NSString *cityName =  [placemark.addressDictionary valueForKey:@"City"];
			 NSString *stateName = [placemark.addressDictionary valueForKey:@"State"];
			 NSString *zipCode = [placemark.addressDictionary valueForKey:@"ZIP"];
			 
			 //perform updates on main thread
			 [[NSOperationQueue mainQueue] addOperationWithBlock:^{
				 
			 _addressLabel.text = [placemark.addressDictionary valueForKey:@"Name"];
			 _addressLabel2.text = [NSString stringWithFormat:@"%@, %@ %@",cityName,stateName,zipCode];
				
			  }];
		
		 }];
	}];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
