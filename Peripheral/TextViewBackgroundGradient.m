//
//  TextViewBackgroundGradient.m
//  Peripheral
//
//  Created by adambradford on 4/20/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "TextViewBackgroundGradient.h"

@implementation TextViewBackgroundGradient

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// code is from paintcode
- (void)drawRect:(CGRect)rect
{
	//// General Declarations
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	//// Color Declarations
	UIColor* fillColor = [UIColor colorWithRed: 0.485 green: 0.485 blue: 0.485 alpha: 1];
	UIColor* strokeColor = [UIColor colorWithRed: 0.896 green: 0.896 blue: 0.896 alpha: 1];
	
	//// Gradient Declarations
	NSArray* gradientColors = [NSArray arrayWithObjects:
							   (id)strokeColor.CGColor,
							   (id)[UIColor colorWithRed: 0.691 green: 0.691 blue: 0.691 alpha: 1].CGColor,
							   (id)fillColor.CGColor, nil];
	CGFloat gradientLocations[] = {0, 0.43, 1};
	CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);
	
	//// Frames
	CGRect frame = self.bounds;
	
	
	//// Rectangle Drawing
	CGRect rectangleRect = CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame) - 1, CGRectGetWidth(frame) + 1, CGRectGetHeight(frame) + 1);
	UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: rectangleRect];
	CGContextSaveGState(context);
	[rectanglePath addClip];
	CGContextDrawLinearGradient(context, gradient,
								CGPointMake(CGRectGetMidX(rectangleRect), CGRectGetMinY(rectangleRect)),
								CGPointMake(CGRectGetMidX(rectangleRect), CGRectGetMaxY(rectangleRect)),
								0);
	CGContextRestoreGState(context);
	
	
	//// Cleanup
	CGGradientRelease(gradient);
	CGColorSpaceRelease(colorSpace);

}

@end
