//
//  textViewBorder.m
//  Peripheral
//
//  Created by adambradford on 4/20/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "textViewBorder.h"

@implementation textViewBorder

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

//code is from paintcode
- (void)drawRect:(CGRect)rect
{
	//// General Declarations
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	//// Color Declarations
	UIColor* fillColor = [UIColor colorWithRed: 0.954 green: 0.954 blue: 0.954 alpha: 1];
	UIColor* strokeColor = [UIColor colorWithRed: 0.686 green: 0.686 blue: 0.686 alpha: 1];
	UIColor* shadowColor2 = [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 1];
	
	//// Shadow Declarations
	UIColor* shadow = shadowColor2;
	CGSize shadowOffset = CGSizeMake(0.1, 1.1);
	CGFloat shadowBlurRadius = 5;
	
	//// Frames
	CGRect frame = self.bounds;
	
	
	//// Rounded Rectangle Drawing
	UIBezierPath* roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(54.5, 32.5, 0, 0) cornerRadius: 0];
	[fillColor setFill];
	[roundedRectanglePath fill];
	[strokeColor setStroke];
	roundedRectanglePath.lineWidth = 1;
	[roundedRectanglePath stroke];
	
	
	//// Rounded Rectangle 2 Drawing
	UIBezierPath* roundedRectangle2Path = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(CGRectGetMinX(frame) + 1.5, CGRectGetMinY(frame) + 1.5, CGRectGetWidth(frame) - 2, CGRectGetHeight(frame) - 3) cornerRadius: 12.5];
	[fillColor setFill];
	[roundedRectangle2Path fill];
	
	////// Rounded Rectangle 2 Inner Shadow
	CGRect roundedRectangle2BorderRect = CGRectInset([roundedRectangle2Path bounds], -shadowBlurRadius, -shadowBlurRadius);
	roundedRectangle2BorderRect = CGRectOffset(roundedRectangle2BorderRect, -shadowOffset.width, -shadowOffset.height);
	roundedRectangle2BorderRect = CGRectInset(CGRectUnion(roundedRectangle2BorderRect, [roundedRectangle2Path bounds]), -1, -1);
	
	UIBezierPath* roundedRectangle2NegativePath = [UIBezierPath bezierPathWithRect: roundedRectangle2BorderRect];
	[roundedRectangle2NegativePath appendPath: roundedRectangle2Path];
	roundedRectangle2NegativePath.usesEvenOddFillRule = YES;
	
	CGContextSaveGState(context);
	{
		CGFloat xOffset = shadowOffset.width + round(roundedRectangle2BorderRect.size.width);
		CGFloat yOffset = shadowOffset.height;
		CGContextSetShadowWithColor(context,
									CGSizeMake(xOffset + copysign(0.1, xOffset), yOffset + copysign(0.1, yOffset)),
									shadowBlurRadius,
									shadow.CGColor);
		
		[roundedRectangle2Path addClip];
		CGAffineTransform transform = CGAffineTransformMakeTranslation(-round(roundedRectangle2BorderRect.size.width), 0);
		[roundedRectangle2NegativePath applyTransform: transform];
		[[UIColor grayColor] setFill];
		[roundedRectangle2NegativePath fill];
	}
	CGContextRestoreGState(context);
	
	[shadowColor2 setStroke];
	roundedRectangle2Path.lineWidth = 1;
	[roundedRectangle2Path stroke];

}


@end
