//
//  ColorChangeButton.h
//  BluChat
//
//  Created by adambradford on 4/22/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorChangeButton : UIButton

@property UIColor *color;

@end
