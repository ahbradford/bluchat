//
//  StopButton.m
//  Peripheral
//
//  Created by adambradford on 4/21/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "SendButton.h"

@implementation SendButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

//Code is from paintcode
- (void)drawRect:(CGRect)rect
{
	//// General Declarations
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	//// Color Declarations
	UIColor* strokeColor = [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 1];
	UIColor* color = [UIColor colorWithRed: 0.467 green: 0.646 blue: 0.9 alpha: 1];
	CGFloat colorRGBA[4];
	[color getRed: &colorRGBA[0] green: &colorRGBA[1] blue: &colorRGBA[2] alpha: &colorRGBA[3]];
	
	UIColor* hightlight = [UIColor colorWithRed: (colorRGBA[0] * 0.4 + 0.6) green: (colorRGBA[1] * 0.4 + 0.6) blue: (colorRGBA[2] * 0.4 + 0.6) alpha: (colorRGBA[3] * 0.4 + 0.6)];
	
	//// Gradient Declarations
	NSArray* gradient3Colors = [NSArray arrayWithObjects:
								(id)hightlight.CGColor,
								
								(id)color.CGColor, nil];
	CGFloat gradient3Locations[] = {0, 0.65};
	CGGradientRef gradient3 = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradient3Colors, gradient3Locations);
	
	//// Shadow Declarations
	UIColor* shadow = strokeColor;
	CGSize shadowOffset = CGSizeMake(0.1, -0.1);
	CGFloat shadowBlurRadius = 2;
	
	//// Frames
	CGRect frame = self.bounds;
	
	
	//// Rounded Rectangle Drawing
	UIBezierPath* roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame) + 4, CGRectGetWidth(frame) - 1.5, CGRectGetHeight(frame) - 9) cornerRadius: 14];
	CGContextSaveGState(context);
	CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, shadow.CGColor);
	[color setFill];
	[roundedRectanglePath fill];
	CGContextRestoreGState(context);
	
	
	
	//// Rounded Rectangle 2 Drawing
	CGRect roundedRectangle2Rect = CGRectMake(CGRectGetMinX(frame) + 9, CGRectGetMinY(frame) + 7, CGRectGetWidth(frame) - 19, CGRectGetHeight(frame) - 20);
	UIBezierPath* roundedRectangle2Path = [UIBezierPath bezierPathWithRoundedRect: roundedRectangle2Rect cornerRadius: 8.5];
	CGContextSaveGState(context);
	[roundedRectangle2Path addClip];
	CGContextDrawLinearGradient(context, gradient3,
								CGPointMake(CGRectGetMidX(roundedRectangle2Rect), CGRectGetMinY(roundedRectangle2Rect)),
								CGPointMake(CGRectGetMidX(roundedRectangle2Rect), CGRectGetMaxY(roundedRectangle2Rect)),
								0);
	CGContextRestoreGState(context);
	
	
	//// Cleanup
	CGGradientRelease(gradient3);
	CGColorSpaceRelease(colorSpace);
	

	

}


@end
