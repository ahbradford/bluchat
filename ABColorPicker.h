//
//  ABColorPicker.h
//  ColorPicker
//
//  Created by adambradford on 2/1/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABColorField.h" 
@interface ABColorPicker : UIControl <ABColorFieldDelegate>

-(void)updateSelectedColor:(UIColor *)color;
-(void)updateCrValue:(int)cR;
-(void)updateCbVAlue:(int)cB;

@property (nonatomic) UIColor *color;
@property (nonatomic) UIColor *previousColor;

@end
