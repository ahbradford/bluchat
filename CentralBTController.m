//
//  CentralBTController.m
//  Quarters
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <CoreBluetooth/CoreBluetooth.h>
#import "TransferService.h"
#import "CentralBTController.h"
#import "PeripheralState.h"

@interface CentralBTController () <CBCentralManagerDelegate, CBPeripheralDelegate>


@property (strong, nonatomic) CBCentralManager      *centralManager;
@property (strong, nonatomic) NSMutableData         *data;
@property (strong, nonatomic) NSMutableSet          *peripherialSet;
@property (strong)            NSMutableDictionary   *currentReceivesinProgress;
@property                       CBCharacteristic   *characteristic;
@property (strong,nonatomic)    NSMutableSet     *UUIDset;
@property (strong,nonatomic)	NSMutableDictionary *peripheralStringDictionary;
@end



@implementation CentralBTController



-(id)init
{
    self = [super init];
    if(self)
    {
        _centralManager = [[CBCentralManager alloc]initWithDelegate:self queue:nil];
		_centralManager.delegate = self;
        
		_peripheralStringDictionary = [NSMutableDictionary dictionary];
        _peripherialSet = [[NSMutableSet alloc]init];
        _currentReceivesinProgress = [[NSMutableDictionary alloc]init];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSData *data = [defaults objectForKey:@"uuidSet"];
        NSSet *uset = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        _UUIDset = [uset mutableCopy];
        if(!_UUIDset) _UUIDset = [NSMutableSet set];
        
		// NSLog(@"%@", _UUIDset);
	
    }
    return self;
}

#pragma mark CBCentralManagerDelegate methods

//check update state, we are waiting for powered on state, if the device does not support BTLE then nothing
//will happen.
-(void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if(central.state == CBCentralManagerStateUnsupported) return;
    if(central.state == CBCentralManagerStatePoweredOn)
    {
        
        
		[self scan];
       
    }
}


//Scan for peripherals that are using our UUID.
-(void)scan
{
    [_centralManager scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]]
                                            options:@{CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
    NSLog(@"Scanning Started");
    
		

}

/** This callback comes whenever a peripheral that is advertising the TRANSFER_SERVICE_UUID is discovered.
 *  We check the RSSI, to make sure it's close enough that we're interested in it, and if it is,
 *  we start the connection process
 */

//Called when a peripherail that is advertising our UUID is discovered, it is then check to see roughy how close it is based
//on power, then we wil connect to it.
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
	[_centralManager cancelPeripheralConnection:peripheral];
	//NSLog(@"%@",[advertisementData objectForKey:CBAdvertisementDataServiceUUIDsKey]);

	NSString *s = [advertisementData objectForKey:CBAdvertisementDataLocalNameKey];
	//if(s != nil && ![s isEqualToString:@"EOM"])
		//NSLog(@"%@",s);
	
	PeripheralState *state = [_peripheralStringDictionary objectForKey:[NSValue valueWithPointer:&peripheral]];
	
	if(![_peripherialSet containsObject:peripheral])
	{
		NSLog(@"New Peripheral Found");
	}
	
	[_peripherialSet addObject:peripheral];
	
	
	
	if(!state && [s characterAtIndex:0] == 3)return;
	
	if(state)
	{
		if([state.lastReceivedTransmission isEqualToString:s])
		{
			return;
		}
		else if ([s characterAtIndex:0] == 3)
		{
			NSRange redRange = NSMakeRange(1, 3);
			NSRange greenRange = NSMakeRange(4, 3);
			NSRange blueRange = NSMakeRange(7, 3);

			float r = [[s substringWithRange:redRange]integerValue]/256.0f;
			float g = [[s substringWithRange:greenRange]integerValue]/256.0f;
			float b = [[s substringWithRange:blueRange]integerValue]/256.0f;
		
			
			[_delegate dataReceivedfromPeripherial:[state.currentComptedString dataUsingEncoding:NSUTF8StringEncoding] withColorR:r colorG:g colorB:b];
			 [_peripheralStringDictionary removeObjectForKey:[NSValue valueWithPointer:&peripheral]];
			
			return;
		}
		else if(s)
		{
			state.currentComptedString = [state.currentComptedString stringByAppendingString:s];
			state.lastReceivedTransmission = s;
			return;
		}
	}
	else if(s)
	{
		PeripheralState *newState = [[PeripheralState alloc]init];
		newState.currentComptedString = s;
		newState.lastReceivedTransmission = s;
		newState.peripheral = peripheral;
		newState.eomReceived = NO;
		[_peripheralStringDictionary setObject:newState forKey:[NSValue valueWithPointer:&peripheral]];
	}


}





@end