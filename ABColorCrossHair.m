//
//  ABColorCrossHair.m
//  ColorPicker
//
//  Created by adambradford on 2/1/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "ABColorCrossHair.h"



#

;
@interface ABColorCrossHair()




@end


@implementation ABColorCrossHair

@synthesize currentLocation = _currentLocation;
-(void)setCurrentLocation:(CGPoint)currentLocation
{
 
  if(currentLocation.x < -10)currentLocation.x = -10;
  if(currentLocation.x > self.bounds.size.width -11)currentLocation.x = self.bounds.size.width -11;
  if(currentLocation.y < -10)currentLocation.y = -10;
  if(currentLocation.y > self.bounds.size.height -11)currentLocation.y = self.bounds.size.height -11;
  
   _currentLocation = currentLocation;
}

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    
    _currentLocation = CGPointMake(30.0f, 30.0f);
    
    self.backgroundColor = [UIColor clearColor];
    
    
  }
  return self;
}

-(id)init
{
	return [self initWithFrame:CGRectZero];
}


- (void)drawRect:(CGRect)rect
{
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextAddEllipseInRect(context, CGRectMake(_currentLocation.x, _currentLocation.y, 20, 20));
  [[UIColor clearColor] setFill];
  [[UIColor blackColor]setStroke];
  CGContextDrawPath(context, kCGPathFillStroke);
  
  CGContextMoveToPoint(context, 0, _currentLocation.y+10);
  CGContextAddLineToPoint(context, _currentLocation.x , _currentLocation.y+10);
  
  CGContextMoveToPoint(context, _currentLocation.x+20, _currentLocation.y+10);
  CGContextAddLineToPoint(context, self.bounds.size.width , _currentLocation.y+10);
  
  CGContextMoveToPoint(context, _currentLocation.x +10, 0);
  CGContextAddLineToPoint(context, _currentLocation.x +10 , _currentLocation.y);
  
  CGContextMoveToPoint(context, _currentLocation.x+10, _currentLocation.y+20);
  CGContextAddLineToPoint(context, _currentLocation.x+10 , self.bounds.size.height);
  
  CGContextSetLineWidth(context, .5);
  
  CGContextDrawPath(context, kCGPathStroke);
  
}



@end

