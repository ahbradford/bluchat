//
//  BubbleViewCell.m
//  Peripheral
//
//  Created by adambradford on 4/20/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "BubbleViewCell.h"

@implementation BubbleViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
