//
//  BubbleViewCell.h
//  Peripheral
//
//  Created by adambradford on 4/20/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BubbleView;

@interface BubbleViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet BubbleView *leftBubble;
@property (weak, nonatomic) IBOutlet UILabel *leftText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftBubbleConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightBubbleConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightLabelConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftLabelConstraint;

@end
