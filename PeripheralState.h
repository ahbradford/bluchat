//
//  PeripheralState.h
//  Peripheral
//
//  Created by adambradford on 4/7/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
@interface PeripheralState : UIView

@property (nonatomic,strong)CBPeripheral	*peripheral;
@property (nonatomic,strong)NSString		*lastReceivedTransmission;
@property (nonatomic,strong)NSString		*currentComptedString;
@property BOOL								eomReceived;

@end
