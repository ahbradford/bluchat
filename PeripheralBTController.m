//
//  PeripheralBTController.m
//  Quarters
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "PeripheralBTController.h"

#import <CoreBluetooth/CoreBluetooth.h>
#import "TransferService.h"



@interface PeripheralBTController () <CBPeripheralManagerDelegate>
@property (strong, nonatomic) CBPeripheralManager       *peripheralManager;
@property (strong, nonatomic) CBMutableCharacteristic   *quarterThrowCharacteristic;
@property (strong, nonatomic) CBMutableCharacteristic   *scoreCharacteristic;
@property (strong, nonatomic) CBMutableCharacteristic   *playerNameCharacteristic;
@property (nonatomic) BOOL                              isConnected;
@property (nonatomic) BOOL                              sendingEOM;
@property (strong,nonatomic) NSData                     *dataToSend;
@property (nonatomic) NSInteger                         sendDataIndex;
@property (atomic,strong)NSMutableSet					*connectedCentrals;
@property (strong, nonatomic)NSString					*messageCharacteristic;
@property double										sleepTimeInterval;
;


@end

#define NOTIFY_MTU      25

@implementation PeripheralBTController

-(id)init
{
    self = [super init];
    if(self)
        
    {
        _peripheralManager = [[CBPeripheralManager alloc]initWithDelegate:self queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
        _isConnected = NO;
		_connectedCentrals = [[NSMutableSet alloc]init];
		[_peripheralManager removeAllServices];
		
		
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		_messageCharacteristic = [defaults objectForKey:@"messageCharacteristic"];
		
		_sleepTimeInterval = .8;
		
		if(!_messageCharacteristic)
		{
			CFUUIDRef theUUID = CFUUIDCreate(NULL);
			CFStringRef string = CFUUIDCreateString(NULL, theUUID);
			_messageCharacteristic =  (__bridge_transfer NSString *)string;
			[defaults setObject:_messageCharacteristic forKey:@"messageCharacteristic"];
		}
		
	//NSLog(@"messageCharacteristic UUID: %@",_messageCharacteristic);
				
    }
    return self;
}

#pragma mark - CBPeripheralManagerDelegate Methods

/** Required protocol method.  A full app should take care of all the possible states,
 *  but we're just waiting for  to know when the CBPeripheralManager is ready
 */
- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    // Opt out from any other state
    if (peripheral.state != CBPeripheralManagerStatePoweredOn) {
        return;
    }
    
    // We're in CBPeripheralManagerStatePoweredOn state...
    NSLog(@"self.peripheralManager powered on.");
    
    // ... so build our service.
 
    // Then the service
    CBMutableService *transferService = [[CBMutableService alloc] initWithType:[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]
                                                                       primary:YES];
    

    // And add it to the peripheral manager
    [self.peripheralManager addService:transferService];

    
	//Start Advertising
	[self.peripheralManager startAdvertising:@{ CBAdvertisementDataLocalNameKey : @"Blank String"}];
    
  
}




/** Sends the next amount of data to the connected central
 */
- (void)sendData
{
	[_peripheralManager stopAdvertising];
    // First up, check if we're meant to be sending an EOM
    static BOOL sendingEOM = NO;
    
    if (sendingEOM)
	{
        
        // send it
		
        [self.peripheralManager startAdvertising:@{ CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]],
				CBAdvertisementDataLocalNameKey : @""}];
		
		
		[NSThread sleepForTimeInterval:2*_sleepTimeInterval];
		

        return;
    }
    
    // We're not sending an EOM, so we're sending data
    
    // Is there any left to send?
    
    if (self.sendDataIndex >= self.dataToSend.length) {
        
        // No data left.  Do nothing
        return;
    }
    
    // There's data left, so send until the callback fails, or we're done.
    
    BOOL didSend = YES;
    
    while (didSend) {
        
        // Make the next chunk
        
        // Work out how big it should be
        NSInteger amountToSend = self.dataToSend.length - self.sendDataIndex;
        
        // Can't be longer than 20 bytes
        if (amountToSend > NOTIFY_MTU) amountToSend = NOTIFY_MTU;
        
        // Copy out the data we want
        NSData *chunk = [NSData dataWithBytes:self.dataToSend.bytes+self.sendDataIndex length:amountToSend];
        
        // Send it
		//NSString *chunkString = [[NSString alloc]initWithData:chunk encoding:NSUTF8StringEncoding];
		//NSLog(@"%@",chunkString);
		[_peripheralManager stopAdvertising];
		
         [self.peripheralManager startAdvertising:@{ CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]],
						  CBAdvertisementDataLocalNameKey : [[NSString alloc]initWithData:chunk encoding:NSUTF8StringEncoding] }];
		[NSThread sleepForTimeInterval:_sleepTimeInterval];
        didSend = YES;
       
        
       
        //NSLog(@"Sent: %@", stringFromData);
        
        // It did send, so update our index
        self.sendDataIndex += amountToSend;
        
        // Was it the last one?
        if (self.sendDataIndex >= self.dataToSend.length) {
            
			[_peripheralManager stopAdvertising];
			[NSThread sleepForTimeInterval:_sleepTimeInterval];
            // It was - send an EOM
            
            // Set this so if the send fails, we'll send it next time
            sendingEOM = YES;
            
            // Send it
            BOOL eomSent = YES;
			
			NSString *eomString;
			
			int red,green,blue;
			char endchar = 3;
			if(_bubbleColor)
			{
				const float* colors = CGColorGetComponents( _bubbleColor.CGColor );
				
				red = colors[0]*255;
				green = colors[1]*255;
				blue = colors[2]*255;
			}
			else
			{
				//default is a nice blue color
				red = 200;
				green = 122;
				blue = 184;
			}
			
			
						
			
			eomString = [NSString stringWithFormat:@"%c",endchar];
			eomString = [eomString stringByAppendingFormat:@"%3d%3d%3d",red,green,blue];
			
			[self.peripheralManager startAdvertising:@{ CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]],
								   CBAdvertisementDataLocalNameKey : eomString}];
			
			
			[NSThread sleepForTimeInterval:_sleepTimeInterval];
			
			[_peripheralManager stopAdvertising];
            [self.peripheralManager startAdvertising:@{ CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]],
					CBAdvertisementDataLocalNameKey : @""}];
            if (eomSent) {
                // It sent, we're all done
                sendingEOM = NO;
                
                //NSLog(@"Sent: EOM");
                _sendDataIndex = 0;
            }
            
            return;
        }
    }
}



-(void)removeServices
{
	[_peripheralManager removeAllServices];
}


#pragma mark PeripheralBTController methods
-(void)stopAdvertising
{
    [_peripheralManager stopAdvertising];
}

-(void)sendDatatoCentral:(NSData *)d
  
{
    self.dataToSend = d;
	[self sendData];
}



@end
