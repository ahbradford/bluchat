//
//  ABColorField.h
//  ColorPicker
//
//  Created by adambradford on 2/1/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABColorFieldDelegate <NSObject>

@required

-(void)updateSelectedColor:(UIColor *)color;
-(void)updateCrValue:(int)cR;
-(void)updateCbVAlue:(int)cB;


@end

@interface ABColorField : UIView

@property (nonatomic) UIColor *currentColor;
@property (nonatomic, weak) id <ABColorFieldDelegate> delegate;
@property double yValue;
@property int rectSize;
@property (nonatomic) UIColor *color;
@property (nonatomic) UIColor *previousColor;

@end
