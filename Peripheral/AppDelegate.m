//
//  AppDelegate.m
//  Quarters
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions

{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];

    // iphone and ipad use the same xib
   
	self.viewController = [[ViewController alloc] initWithNibName:@"ViewController_iPhone" bundle:nil];
    
	//create navagation conroller
	UINavigationController *navagationController = [[UINavigationController alloc]init];

	//stop app from timing out
	[UIApplication sharedApplication].idleTimerDisabled = YES;
	
	//more navagation controller stuff
    self.window.rootViewController = navagationController;
	
	[navagationController pushViewController:_viewController animated:NO];
	
	
    [self.window makeKeyAndVisible];
    
	//get defaults and set variables
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _viewController.nameTextField.text = [defaults objectForKey:@"userName"];
	
	NSNumber *red = [defaults objectForKey:@"red"];
	NSNumber *green = [defaults objectForKey:@"green"];
	NSNumber *blue = [defaults objectForKey:@"blue"];
	if(!(red.floatValue == 0.0 && green.floatValue  ==  0.0 && blue.floatValue == 0))
	{
		_viewController.bubbleColor = [UIColor colorWithRed:red.floatValue green:green.floatValue blue:blue.floatValue alpha:1.0];
	}

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	//save defaults and shut down services
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:_viewController.nameTextField.text forKey:@"userName"];
	
	if(_viewController.bubbleColor)
	{
		const float* colors = CGColorGetComponents( _viewController.bubbleColor.CGColor );
		[defaults setValue:[NSNumber numberWithFloat:colors[0]] forKey:@"red"];
		[defaults setValue:[NSNumber numberWithFloat:colors[1]] forKey:@"green"];
		[defaults setValue:[NSNumber numberWithFloat:colors[2]] forKey:@"blue"];
	}
	[_viewController shutDownServices];
	
	
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	//shut down services
	[_viewController shutDownServices];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    //reinitiallize servcies
	[_viewController reInitalizeBluetoothAndLocation];
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _viewController.nameTextField.text = [defaults objectForKey:@"userName"];
	
	NSNumber *red = [defaults objectForKey:@"red"];
	NSNumber *green = [defaults objectForKey:@"green"];
	NSNumber *blue = [defaults objectForKey:@"blue"];
	if(!(red.floatValue == 0.0 || green.floatValue  ==  0.0 || blue.floatValue == 0))
	{
		_viewController.bubbleColor = [UIColor colorWithRed:red.floatValue green:green.floatValue blue:blue.floatValue alpha:1.0];
	}
	
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
	
}

@end
