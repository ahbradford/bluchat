//
//  BubbleView.h
//  Peripheral
//
//  Created by adambradford on 4/20/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BubbleView : UIView

@property UIColor *color;

//0 for left, not 0 for right;
@property int orientation;

@end
