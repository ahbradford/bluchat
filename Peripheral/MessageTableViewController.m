//
//  messageTableViewController.m
//  Peripheral
//
//  Created by adambradford on 4/7/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "MessageTableViewController.h"
#import "Message.h"
#import "MapViewController.h"
#import "BubbleViewCell.h"
#import "BubbleView.h"

@interface MessageTableViewController ()
@property UIFont *messageFont;

@property MapViewController *map;


@end

@implementation MessageTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
	{
		//set up fonts and create managed object
		_messageFont = [UIFont fontWithName:@"Helvetica Neue" size:14];
		if(!_managedObjectContext)[self openDataModel];

    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	//create managed object if necessary
	if(!_managedObjectContext)[self openDataModel];
	
}

//sets up managed object context
- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    if (managedObjectContext)
	{
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Message"];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES ]];
        request.predicate = nil; // all Photographers
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    } else {
        self.fetchedResultsController = nil;
    }
}

//opens up core database
- (void)openDataModel
{
	//find data file
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    url = [url URLByAppendingPathComponent:@"MessageStore"];
    UIManagedDocument *document = [[UIManagedDocument alloc] initWithFileURL:url];
    
	//create a new file if one does not exist
    if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]])
	{
        [document saveToURL:url
           forSaveOperation:UIDocumentSaveForCreating
          completionHandler:^(BOOL success)
		 {
			 if (success) {
				 self.managedObjectContext = document.managedObjectContext;
				 
			 }
		 }];
		//open file if closed
    } else if (document.documentState == UIDocumentStateClosed)
	{
        [document openWithCompletionHandler:^(BOOL success)
		 {
			 if (success) {
				 self.managedObjectContext = document.managedObjectContext;
			 }
		 }];
    } else {
        self.managedObjectContext = document.managedObjectContext;
    }
	
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	//get our message and stop 80 pixels from the left
	Message *message = [self.fetchedResultsController objectAtIndexPath:indexPath];
	CGSize constraintSize = CGSizeMake(self.view.frame.size.width-80, MAXFLOAT);
	
	//see how long our string would be
	CGSize size = [message.messageString sizeWithFont:_messageFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
	
	// these numbers seem to look good.  No real formula here, just trial and error...
	return   size.height +26;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//create our cell from xib
    static NSString *CellIdentifier = @"Cell";
    BubbleViewCell *cell = (BubbleViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell==nil)
	{
        NSArray *xib = [[NSBundle mainBundle] loadNibNamed:@"BubbleViewCell" owner:self options:nil];
		cell = xib[0];
		
    }
	
	//no selection indicators
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	//get our message object
	Message *message = [self.fetchedResultsController objectAtIndexPath:indexPath];
	
	//check if our message was recieved, or sent to determine bubble location/orientation
	BOOL received = message.received;
	
	//set up for a received message
	if(received == 1)
	{

		CGSize maxBubbleSize = CGSizeMake(tableView.frame.size.width -50, [self tableView:self.tableView heightForRowAtIndexPath:indexPath]);
		
		CGSize stringsize = [message.messageString sizeWithFont:_messageFont constrainedToSize:maxBubbleSize lineBreakMode:NSLineBreakByWordWrapping];
		
		cell.leftBubble.color = [UIColor colorWithRed: message.red green: message.green blue: message.blue alpha: 1]; ;
		cell.leftBubble.orientation = 1;
		cell.leftText.text =  message.messageString;
		cell.leftText.font = _messageFont;
		cell.leftText.numberOfLines = 0;
		cell.leftText.lineBreakMode = NSLineBreakByWordWrapping;
		
		cell.leftLabelConstraint.constant = 20;
		cell.rightBubbleConstraint.constant = tableView.frame.size.width - stringsize.width - 40;
		
		cell.contentMode = UIViewContentModeRedraw;
		
	}
	//set up for a sent message
	else
	{
		
		CGSize maxBubbleSize = CGSizeMake(tableView.frame.size.width -80 , [self tableView:self.tableView heightForRowAtIndexPath:indexPath]-10 );
		
		CGSize stringsize = [message.messageString sizeWithFont:_messageFont constrainedToSize:maxBubbleSize lineBreakMode:NSLineBreakByWordWrapping];
		
		cell.leftBubble.orientation = 0;
		if(!(message.red == 0.0 && message.green == 0.0 && message.blue == 0.0))
		{
			cell.leftBubble.color = [UIColor colorWithRed: message.red green: message.green blue: message.blue alpha: 1];
		}
		else
		{
			cell.leftBubble.color = [UIColor colorWithRed: 0.467 green: 0.646 blue: 0.9 alpha: 1];
		}
		cell.leftText.text =  message.messageString;
		cell.leftText.font = _messageFont;
		cell.leftText.numberOfLines = 0;
		cell.leftText.textAlignment = NSTextAlignmentRight;
		cell.leftText.lineBreakMode = NSLineBreakByWordWrapping;
	
		cell.leftBubbleConstraint.constant = tableView.frame.size.width - stringsize.width - 32;
		cell.rightLabelConstraint.constant = 20;
		
		cell.contentMode = UIViewContentModeRedraw;
		
	}

    return cell;
	
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	//this part just checks if the keyboard is visible, if it is it just hides it and returns
	if([_textView1 isFirstResponder])
	{
		[_textView1 resignFirstResponder];
		return;
	}
	if([_textView2 isFirstResponder])
	{
		[_textView2 resignFirstResponder];
		return;
	}
	
	//create our map if necessary
	if(!_map)
	{
		_map = [[MapViewController alloc] initWithNibName:@"MapView_iPhone" bundle:nil];
	}

	//get our message
	Message *message = [self.fetchedResultsController objectAtIndexPath:indexPath];
	
	_map.location = [[CLLocation alloc]initWithLatitude:message.latitude.doubleValue longitude:message.longitude.doubleValue];
	_map.date = message.date;
	_map.message = message.messageString;
	
	//push map onto screen
	[_currentNavagationController pushViewController:_map animated:YES];
}

@end
