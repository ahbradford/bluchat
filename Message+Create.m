//
//  Message+Create.m
//  Peripheral
//
//  Created by adambradford on 4/7/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "Message+Create.h"

@implementation Message (Create)

+ (Message *)messageWithText:(NSString *)text received:(BOOL)received latitude:(float)latitude longitude:(float)longiutde date:(NSDate*)date red:(float)r green:(float)g blue:(float)b inManagedObjectContext:(NSManagedObjectContext *)context;
{
	Message *message = nil;
	
	message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:context];
	message.messageString = text;
	message.latitude = [NSNumber numberWithFloat:latitude];
	message.longitude = [NSNumber numberWithFloat:longiutde];
	message.date = date;
	message.received = received;
	message.red = r;
	message.green = g;
	message.blue = b;
	
	
	return message;
	
}

@end
