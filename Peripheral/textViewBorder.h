//
//  textViewBorder.h
//  creates a custom text field background
//
//  Created by adambradford on 4/20/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface textViewBorder : UIView

@end
