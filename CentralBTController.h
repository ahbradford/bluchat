//
//  CentralBTController.h
//  Quarters
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

@protocol CentralBTControllerDelegate <NSObject>
-(void)dataReceivedfromPeripherial:(NSData *)data withColorR:(float)r colorG:(float)g colorB:(float)b;


@end
#import <Foundation/Foundation.h>
@class CBPeripheral;

@interface CentralBTController : NSObject


@property id<CentralBTControllerDelegate> delegate;




@end
