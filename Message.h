//
//  Message.h
//  A message that is handed by core data
//
//  Created by adambradford on 4/7/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Message : NSManagedObject

@property (nonatomic, retain) NSString * messageString;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic) BOOL received;
@property (nonatomic)float red;
@property (nonatomic)float green;
@property (nonatomic)float blue;
@end
