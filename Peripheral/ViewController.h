//
//  ViewController.h
//  
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SendButton,textViewBorder,ColorChangeButton;


@interface ViewController : UIViewController 


@property (weak, nonatomic) IBOutlet ColorChangeButton *colorSettingsButton;
@property (strong,nonatomic) UIColor						 *bubbleColor;
@property (weak, nonatomic) IBOutlet UITextField			 *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField			 *textField;
@property (weak, nonatomic) IBOutlet UITableView			 *messageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton				 *infoButton;
@property (weak, nonatomic) IBOutlet UIButton				 *sendButton;
@property (weak, nonatomic) IBOutlet UIView					 *textViewBackground;
@property (weak, nonatomic) IBOutlet UIView					 *textViewBorder;

//constraint to adjust when keyboard appears
@property (weak, nonatomic) IBOutlet NSLayoutConstraint		 *keyboardHeight;

- (IBAction)settingsButtonPressed:(id)sender;
- (IBAction)infoButtonPressed:(id)sender;
- (IBAction)send;
- (void)reInitalizeBluetoothAndLocation;
- (void)shutDownServices;


@end
