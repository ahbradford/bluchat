//
//  messageTableViewController.h
//  Peripheral
//
//  Created by adambradford on 4/7/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"


@interface MessageTableViewController : CoreDataTableViewController

@property (strong,nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak,nonatomic) UINavigationController *currentNavagationController;
@property (weak,nonatomic) UITextField *textView1;
@property (weak,nonatomic) UITextField *textView2;


@end
