//
//  Message+Create.h
//  Catagory that creates a message a convience constructor for a Message object
//
//  Created by adambradford on 4/7/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "Message.h"

@interface Message (Create)

+ (Message *)messageWithText:(NSString *)text received:(BOOL)received latitude:(float)latitude longitude:(float)longiutde date:(NSDate*)date red:(float)r green:(float)g blue:(float)b inManagedObjectContext:(NSManagedObjectContext *)context;

@end
