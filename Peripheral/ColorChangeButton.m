//
//  ColorChangeButton.m
//  BluChat
//
//  Created by adambradford on 4/22/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "ColorChangeButton.h"

@implementation ColorChangeButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
	//// General Declarations
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	//// Color Declarations
	UIColor* fillColor = self.color;
	UIColor* strokeColor = [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 1];
	UIColor* color = [UIColor colorWithRed: 0.333 green: 0.333 blue: 0.333 alpha: 1];
	
	//// Shadow Declarations
	UIColor* shadow = strokeColor;
	CGSize shadowOffset = CGSizeMake(1.1, 1.1);
	CGFloat shadowBlurRadius = 3;
	
	//// Frames
	CGRect frame = self.bounds;
	
	
	//// Rounded Rectangle Drawing
	UIBezierPath* roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(CGRectGetMinX(frame) + 1.5, CGRectGetMinY(frame) + 1.5, CGRectGetWidth(frame) - 3, CGRectGetHeight(frame) - 3) cornerRadius: 9];
	[fillColor setFill];
	[roundedRectanglePath fill];
	CGContextSaveGState(context);
	CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, shadow.CGColor);
	[color setStroke];
	roundedRectanglePath.lineWidth = 1;
	[roundedRectanglePath stroke];
	CGContextRestoreGState(context);
	
	

}


@end
