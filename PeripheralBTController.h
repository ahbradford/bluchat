//
//  PeripheralBTController.h
//  Quarters
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PeripheralBTController;

@protocol PeripheralBTControllerDelegate <NSObject>





@end

@interface PeripheralBTController : NSObject
-(void)stopAdvertising;
-(void)sendDatatoCentral:(NSData *)data;
-(void)removeServices;

@property id<PeripheralBTControllerDelegate> delegate;
@property UIColor *bubbleColor;


@end
